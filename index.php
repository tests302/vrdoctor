<?php
require_once __DIR__ . '/boot.php';

$user = null;

if (check_auth()) {
  $stmt = pdo()->prepare("SELECT * FROM `users` WHERE `id` = :id");
  $stmt->execute(['id' => $_SESSION['user_id']]);
  $user = $stmt->fetch(PDO::FETCH_ASSOC);
}
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>vrDoctorTest</title>
  <link rel="stylesheet" href="css/bootstrap.css">
</head>
<body>

<div class="container">
  <div class="row py-5">
    <div class="col-lg-6">

      <?php if ($user) { ?>

        <h1><?= htmlspecialchars($user['username']) ?>, вы успешно авторизовались!</h1>

        <form class="mt-5" method="post" action="do_logout.php">
          <button type="submit" class="btn btn-primary">Выход</button>
        </form>


        <form enctype="multipart/form-data" action="import.php" method="POST">
          <div class="form-group">
            <label for="import">Проимпортируйте xml файл</label><br><br>
            <input type="hidden" name="MAX_FILE_SIZE" value="30000"/>
            <input type="file" class="form-control-file" id="import" name="import"><br><br>
            <button type="submit" class="btn btn-primary">Импорт</button>
          </div>
        </form>


        <?php
        if ($_GET['import'] == 'done') {
          echo 'Успешно импортировано';
        }
        ?>

        <form class="mt-5" method="post" action="get_users.php">
          <button type="submit" class="btn btn-primary">Вывести список пользователей, чьи питомцы старше 3 лет</button>
        </form>


      <?php } else { ?>

        <h1 class="mb-5">Регистрация</h1>

        <?php flash(); ?>

        <form method="post" action="do_register.php">
          <div class="mb-3">
            <label for="username" class="form-label">Имя пользователя</label>
            <input type="text" class="form-control" id="username" name="username" required>
          </div>
          <div class="mb-3">
            <label for="password" class="form-label">Пароль</label>
            <input type="password" class="form-control" id="password" name="password" required>
          </div>
          <div class="d-flex justify-content-between">
            <button type="submit" class="btn btn-primary">Регистрация</button>
            <a class="btn btn-outline-primary" href="login.php">Вход</a>
          </div>
        </form>

      <?php } ?>

    </div>
  </div>
</div>

</body>
</html>
CREATE TABLE `users`
(
    `id`       int unsigned                            NOT NULL AUTO_INCREMENT,
    `username` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
    `password` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `username` (`username`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

CREATE TABLE `pets`
(
    `id`         int unsigned                            NOT NULL AUTO_INCREMENT,
    `name`       varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
    `user_id`    int unsigned,
    `birth_date` date,
    PRIMARY KEY (`id`),
    FOREIGN KEY (user_id) REFERENCES users (id) on delete cascade
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;
<?php
require_once __DIR__ . '/boot.php';

$user = null;

if (check_auth()) {
  $stmt = pdo()->prepare("SELECT * FROM `users` WHERE `id` = :id");
  $stmt->execute(['id' => $_SESSION['user_id']]);
  $user = $stmt->fetch(PDO::FETCH_ASSOC);
}

if ($user) {
  $name = "uploads/" . $_FILES["import"]["name"];

  if ($_FILES && $_FILES["import"]["error"] == UPLOAD_ERR_OK) {
    move_uploaded_file($_FILES["import"]["tmp_name"], $name);

    if (file_exists($name)) {
      $pets = simplexml_load_file($name);

      $sql = 'INSERT INTO pets (name, user_id, birth_date) VALUES ';

      foreach ($pets->pet as $pet) {
        $birthDate = date('Y-m-d', strtotime($pet['birth_date']));

        $sql .= "('{$pet}', {$pet['user_id']}, '{$birthDate}'),";
      }

      $sql = substr($sql, 0, -1);

      insert($sql);

      unlink($name);

      header("Location:/?import=done");
    }
  } else {
    exit("Ошибка импорта файла {$name}");
  }
} else {
  exit('Необходимо авторизоваться');
}
<?php
require_once __DIR__ . '/boot.php';

$now = date('Y-m-d');

$sql = "SELECT distinct users.id, username FROM users INNER JOIN pets ON (pets.`birth_date` >= {$now})";

$sth = pdo()->prepare($sql);
$sth->execute();
$result = $sth->fetchAll(PDO::FETCH_ASSOC);

if (!empty($result)) {
  foreach ($result as $user) {
    if (!empty($user)) {
      foreach ($user as $field) {
        echo "{$field} ";
      }
      echo "<br>";
    }
  }
}